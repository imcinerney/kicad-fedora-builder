#!/bin/sh

set -e
set -x

dnf install git rpm-build copr-cli -y
dnf remove python3-progress --noautoremove -y
./builder.sh -c "@kicad/kicad"

exit 0
